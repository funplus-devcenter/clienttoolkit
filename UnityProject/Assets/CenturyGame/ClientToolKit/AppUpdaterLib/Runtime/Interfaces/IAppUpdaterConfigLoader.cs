﻿using CenturyGame.AppUpdaterLib.Runtime.Configs;

namespace CenturyGame.AppUpdaterLib.Runtime.Interfaces
{
    public interface IAppUpdaterConfigLoader
    {
        AppUpdaterConfig Load();
    }
}