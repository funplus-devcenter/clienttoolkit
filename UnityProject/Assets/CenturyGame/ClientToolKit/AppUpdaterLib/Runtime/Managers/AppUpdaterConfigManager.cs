/***************************************************************

 *  类名称：        AppUpdaterConfigManager

 *  描述：				

 *  作者：          Chico(wuyuanbing)

 *  创建时间：      2021/2/24 16:32:50

 *  最后修改人：

 *  版权所有 （C）:   CenturyGames

***************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CenturyGame.AppUpdaterLib.Runtime.Configs;
using CenturyGame.AppUpdaterLib.Runtime.Interfaces;
using CenturyGame.AppUpdaterLib.Runtime.Configs.Loader;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace CenturyGame.AppUpdaterLib.Runtime.Managers
{
    public class AppUpdaterConfigManager
    {
        //--------------------------------------------------------------
        #region Fields
        //--------------------------------------------------------------

        /// <summary>
        /// 默认Loader
        /// </summary>
        private static IAppUpdaterConfigLoader defaultLoader = new AppUpdaterConfigLoader();

        /// <summary>
        /// 自定义Loader
        /// </summary>
        private static IAppUpdaterConfigLoader customLoader;
        #endregion

        //--------------------------------------------------------------
        #region Properties & Events
        //--------------------------------------------------------------

        private static AppUpdaterConfig mAppUpdaterConfig;

        public static AppUpdaterConfig AppUpdaterConfig
        {
            get
            {
                if (mAppUpdaterConfig == null)
                {
                    if (customLoader == null)
                    {
                        mAppUpdaterConfig = defaultLoader.Load();
                    }
                    else
                    {
                        mAppUpdaterConfig = customLoader.Load();
                    }
                }

                return mAppUpdaterConfig;
            }
        }
        #endregion

        //--------------------------------------------------------------
        #region Creation & Cleanup
        //--------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------
        #region Methods
        //--------------------------------------------------------------

        /// <summary>
        /// 设置自定义Loader
        /// </summary>
        /// <param name="_customLoader"></param>
        public static void SetCustomLoader(IAppUpdaterConfigLoader _customLoader)
        {
            customLoader = _customLoader;
        }
        #endregion

    }
}
