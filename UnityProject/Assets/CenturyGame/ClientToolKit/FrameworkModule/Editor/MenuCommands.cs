﻿using System.Text;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace CenturyGame.Framework.Editor
{
    public sealed class MenuCommands
    {
        private const string CopyFrameworkModuleLinkXml = "CenturyGame/FrameworkModule/Copy link.xml to FrameworkModule contents folder";

        [MenuItem(CopyFrameworkModuleLinkXml)]
        public static void CopyLinkXml()
        {
            string path = Path.GetFullPath("Packages/com.centurygame.clienttoolkit/FrameworkModule/link.xml");

            if (!File.Exists(path))
            {
                Debug.LogError("The link.xml that we want to copy is not exist in the current package , try to reinstall package!");
            }

            string targetFolderPath = $"{Application.dataPath}/CenturyGamePackageRes/FrameworkModule";
            if (!Directory.Exists(targetFolderPath))
            {
                Directory.CreateDirectory(targetFolderPath);

                AssetDatabase.Refresh();
            }

            var targetPath = $"{Application.dataPath}/CenturyGamePackageRes/FrameworkModule/link.xml";
            string xml = File.ReadAllText(path, new UTF8Encoding(false, true));
            File.WriteAllText(targetPath, xml, new UTF8Encoding(false, true));
            Debug.Log($"Copy link.xml file success , xml path : {targetPath} .");

            AssetDatabase.Refresh();
        }
    }
}